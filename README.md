<div align="center">
  <h1><code>pluginOne</code></h1>
  <p>
    <strong>Short Description</strong>
  </p>
  <p style="margin-bottom: 0.5ex;">
    <img
        src="https://img.shields.io/github/downloads//pluginOne/total"
    />
    <img
        src="https://img.shields.io/github/last-commit//pluginOne"
    />
    <img
        src="https://img.shields.io/github/issues//pluginOne"
    />
    <img
        src="https://img.shields.io/github/issues-closed//pluginOne"
    />
    <img
        src="https://img.shields.io/github/repo-size//pluginOne"
    />
    <img
        src="https://img.shields.io/github/workflow/status//pluginOne/Compile%20and%20release"
    />
  </p>
</div>


## Requirements ##
- Sourcemod and Metamod


## Installation ##
1. Grab the latest release from the release page and unzip it in your sourcemod folder.
2. Restart the server or type `sm plugins load pluginOne` in the console to load the plugin.
3. The config file will be automatically generated in cfg/sourcemod/

## Configuration ##
- You can modify the phrases in addons/sourcemod/translations/pluginOne.phrases.txt.
- Once the plugin has been loaded, you can modify the cvars in cfg/sourcemod/pluginOne.cfg.


## Usage ##
