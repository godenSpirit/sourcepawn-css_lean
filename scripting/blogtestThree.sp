#include <sdkhooks>
#include <sdktools>
#include <sourcemod>
#pragma newdecls required
#pragma semicolon 1


public Plugin myinfo =
{
	name = "pluginOne",
	author = "",
	description = "",
	version = "1.0.0",
	url = "https://github.com//pluginOne"
};


public void OnPluginStart()
{
	PrintToChatAll("这是第一个测试");
    
    HookEvent("player_death",OnPlayerDeathFunction);
}

//事件触发函数
public void OnPlayerDeathFunction(Event event, const char[] name, bool dontBroadcast){

    //short	userid	user ID who died
    //short	attacker	user ID who killed
    //string	weapon	weapon name killer used
    //bool	headshot	singals a headshot

    int userid = event.GetInt("userid");

    int attacker = event.GetInt("attacker");

    bool isHeadshot = event.GetBool("headshot");


    int clientid = GetClientOfUserId(userid);

    int attackerid = GetClientOfUserId(attacker);

    char victim_name[36],attacker_name[36];

    GetClientName(clientid,victim_name,sizeof(victim_name));

    GetClientName(attackerid,attacker_name,sizeof(attacker_name));

    if(isHeadshot){
        PrintToChatAll(" | %s | kill | %s |  in headshot ",attacker_name,victim_name);
    }else{
        PrintToChatAll(" | %s | kill | %s |  ",attacker_name,victim_name);
    }


}



