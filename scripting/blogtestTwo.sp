#include <sdkhooks>
#include <sdktools>
#include <sourcemod>
#pragma newdecls required
#pragma semicolon 1


public Plugin myinfo =
{
	name = "pluginOne",
	author = "",
	description = "",
	version = "1.0.0",
	url = "https://github.com//pluginOne"
};


public void OnPluginStart()
{
	PrintToChatAll("这是第一个测试");
    
	//注册控制台命令
	RegConsoleCmd("sm_sayhellow",SayHellowToAll,"这是一个测试测试的控制台命令");

    RegConsoleCmd("sm_saysthtoall",SaySthSeveralTimes,"这是一个测试测试的控制台命令");
}

public Action SayHellowToAll(int client,int args)
{
	//向所有人发送信息
    PrintToChatAll("WDNMD");
    
    return Plugin_Handled;
}

public Action SaySthSeveralTimes(int client,int args){

    char clientname[36],args1[64],args2[10];

    if(args!=2){
        PrintToChat(client,"此命令需要两个参数，1为内容，2为要重复的次数");
        return Plugin_Handled;
    }

    GetClientName(client,clientname,sizeof(clientname));

    GetCmdArg(1,args1,sizeof(args1));

    GetCmdArg(2,args2,sizeof(args2));

    int times = StringToInt(args2);

    if(times<1||times>=10){

        PrintToChat(client,"次数必须在10以内！");
        return Plugin_Handled;

    }

    for(int i =0;i<times;i++){
        PrintToChatAll("| %s | say %s To EveryOne",clientname,args1);
    }

    return Plugin_Handled;
}

